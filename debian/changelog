leaflet-markercluster (1.5.3~dfsg-4) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit
  * Apply multi-arch hints
    + libjs-leaflet.markercluster: Add Multi-Arch: foreign

  [ lintian-brush ]
  * Update lintian override info format in d/source/lintian-overrides
    on line 2-5
  * Update standards version to 4.6.2, no changes needed

  [ Yadd ]
  * Mark nodejs dependency with :any
  * Fix rollup configuration for rollup 3 (Closes: #1026699)

 -- Yadd <yadd@debian.org>  Wed, 18 Jan 2023 10:32:11 +0400

leaflet-markercluster (1.5.3~dfsg-3) unstable; urgency=medium

  * fix eslint test
  * tighten lintian overrides

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Feb 2022 00:07:01 +0100

leaflet-markercluster (1.5.3~dfsg-2) unstable; urgency=medium

  * add patch 2003
    to modernize use of NodeJS module rollup-plugin-json;
    tighten build-dependency on node-rollup-plugin-json
  * call executable terser (not uglifyjs.terser),
    and build-depend on terser (not uglifyjs.terser)

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 07 Nov 2021 21:33:05 +0100

leaflet-markercluster (1.5.3~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * fix have autopkgtest depend on node-js-yaml

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 04 Nov 2021 14:38:50 +0100

leaflet-markercluster (1.5.1~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * simplify source helper script copyright-check
  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage
  * use debhelper compatibility level 13 (not 12)
  * declare compliance with Debian Policy 4.6.0
  * drop patch cherry-picked upstream now applied
  * unfuzz patches

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 09 Oct 2021 14:11:38 +0200

leaflet-markercluster (1.4.1~dfsg-10) unstable; urgency=medium

  * add patch cherry-picked from upstream pull request
    to avoid global L using Rollup plugin inject,
    and stop use earlier more invasive yet non-working patch;
    build-depend on node-rollup-plugin-inject;
    closes: bug#987128

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 Apr 2021 12:23:19 +0200

leaflet-markercluster (1.4.1~dfsg-9) unstable; urgency=medium

  * use brotli compression suffix .brotli
    (not .br used for language breton)
  * declare compliance with Debian Policy 4.5.1

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Dec 2020 22:08:03 +0100

leaflet-markercluster (1.4.1~dfsg-8) unstable; urgency=medium

  [ Debian Janitor ]
  * use debhelper compatibility level 12 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * set upstream metadata fields: Repository Repository-Browse

  [ Jonas Smedegaard ]
  * drop superficial domino autopkgtest
    (seemingly confused by ES module syntax);
     add superficial skip-not-installable eslint autopkgtest
  * build ES module; build-depend on uglifyjs.terser

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 14 Jun 2020 09:57:57 +0200

leaflet-markercluster (1.4.1~dfsg-7) unstable; urgency=medium

  * Fix build with recent rollup:
    + Add patch 1001 to modernize rollup config.
    + Explicitly build-depend on node-rollup-plugin-json.
    Closes: Bug#941601. Thanks to Pirate Praveen.
  * Declare compliance with Debian Policy 4.4.1.
  * Install nodejs code under /usr/share (not /usr/lib).
  * Mark autopkgtests as superficial.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 10 Oct 2019 18:33:51 +0200

leaflet-markercluster (1.4.1~dfsg-6) unstable; urgency=medium

  * Fix have libjs-leaflet.markercluster break (not provide)
    libjs-leaflet-markercluster.
    Closes: Bug#924104. Thanks to Andreas Beckmann.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 09 Mar 2019 23:08:34 +0100

leaflet-markercluster (1.4.1~dfsg-5) unstable; urgency=medium

  * Fix provide optimized CSS files.
  * Enable testsuite.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Mar 2019 01:33:48 +0100

leaflet-markercluster (1.4.1~dfsg-4) experimental; urgency=medium

  * Revive libjs-leaflet-markercluster as empty transitional package,
    to ease upgrade to libjs-leaflet.markercluster.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 21 Jan 2019 08:30:26 +0100

leaflet-markercluster (1.4.1~dfsg-3) unstable; urgency=medium

  * Update copyright info: Use https protocol in file format URL.
  * Fix clean generated documentation.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 21 Jan 2019 01:41:51 +0100

leaflet-markercluster (1.4.1~dfsg-2) experimental; urgency=medium

  * Rename binary package to libjs-leaflet.markercluster,
    providing/conflicting with/replacing libjs-leaflet-markercluster.
  * Improve short and long description.
  * Provide generated README.txt and README.html,
    not README.md source.
    Build-depend on pandoc.
  * Add binary package node-leaflet.markercluster.
  * Extend patch 2002 to avoid privacy breaches also in documentation.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 20 Jan 2019 16:00:10 +0100

leaflet-markercluster (1.4.1~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Simplify rules:
    + Stop resolve build-dependencies in rules file.
    + Stop resolve binary package relations in rules file.
    + Use debhelper hint files.
    + Use short-form dh sequencer (not cdbs).
      Stop build-depend on cdbs.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.3.0.
  * Update Vcs-* fields: Maintenance moved to Salsa.
  * Set Rules-Requires-Root: no.
  * Wrap and sort control file.
  * Repurpose patch 2001 to avoid git during build.
  * Add patch 2002 to fix privacy issues in examples.
    Stop mangle examples during install.
    Include mangling helper script with source.
  * Update build rules.
    Build-depend on rename rollup uglifyjs (not node-jake node-uglify).
  * Optimize CSS file.
    Build-depend on sassc.
  * Update and bump version of NEWS entry.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 20 Jan 2019 12:18:19 +0100

leaflet-markercluster (1.2.0~dfsg-1) experimental; urgency=medium

  [ upstream ]
  * New release.
    + Move clusterPane option in to options field.
    + Fix very small maxClusterRadius hanging the browser.

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 4.1.3.
  * Update CDN URL getting replaced with local non-tracking paths in
    example files.
  * Update copyright info:
    + Extend coverage for main upstream author.
    + Extend coverage for myself.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 06 Jan 2018 20:27:30 +0100

leaflet-markercluster (1.1.0~dfsg-1) experimental; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Update watch file: Use substitution strings.
  * Tighten lintian overrides regarding License-Reference.
  * Drop obsolete lintian override regarding debhelper 9.
  * Fix use package section javascript (not web).
  * Update to use package optional (not extra).
  * Modernize Vcs-* fields:
    + Consistently use git (not cgit) in path.
    + Consistently use .git suffix in path.
  * Declare compliance with Debian Policy 4.1.1.
  * Update package relations:
    + Relax to build-depend unversioned on node-uglify: Needed version
      satisfied even in oldstable.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 22 Oct 2017 19:26:32 +0200

leaflet-markercluster (1.0.4~dfsg-1) experimental; urgency=medium

  * Modernize cdbs:
    + Drop CDBS debhelper hint: Handled in CDBS now.
    + Do copyright-check in maintainer script (not during build).
  * Normalize library files to use .min suffix.
  * Tighten build-dependency on libjs-leaflet.
  * Update copyright info:
    + Extend copyright of packaging to cover current year.
  * Add NEWS entry about major release with ABI changes, and normalized
    library filenames.
  * Update package relations:
    + Stop build-depend on devscripts.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 16 Apr 2017 07:37:44 +0200

leaflet-markercluster (0.5.0~dfsg-2) unstable; urgency=medium

  * Update Vcs-Git field: Fix path.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 15 Jun 2016 09:52:40 +0200

leaflet-markercluster (0.5.0~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    Bugfixes:
    + Fix getBounds when removeOutsideVisibleBounds: false is set.
    + Fix zoomToShowLayer fails after initial spiderfy.
    + Fix cluster not disappearing on Android.
    + Fix RemoveLayers() when spiderified.
    + Remove lines from map when removing cluster.
    + Fix getConvexHull when all markers are located at same latitude.
    + Fix removeLayers when cluster is not on the map.
    + Improved zoomToShowLayer with callback check.
    + Improved reliability of RefreshSpec test suite for PhantomJS.
    + Corrected effect of removeOutsideVisibleBounds option.
    + Fix getLayer when provided a string.
    + Documentation improvements.
    + Correct _getExpandedVisibleBounds for Max Latitude.
    + Correct unspiderfy vector.
    + Remove "leaflet-cluster-anim" class on map remove while spiderfied.
    + Fix disableClusteringAtZoom maxZoom troubles.
    Improvements:
    + chunkedLoading option to keep browser more responsive during
      larging a load data set.
    + maxClusterRadius can be a function.
    + Spiderfy without zooming when all markers at same location.
    + On becoming visible, markers retain their original opacity.
    + Spiderleg Polyline options.
    + Extra methods to allow refreshing cluster icons.
    + Ability to disable animations.
    + Optimized performance of bulk addLayers and removeLayers.
    + Replaced spiderfy legs animation from SMIL to CSS transition.
    + Provide more detailed context information on the spiderfied event.
    + Add unspiderfied event.
    + Readme updates.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump file format to version 4.
    + Use github pattern from documentation.
    + Set repacksuffix.
    + Add usage comment.
  * Modernize git-buildpackage config:
    + Avoid git- prefix.
    + Filter any .gitignore file.
  * Use https protocol in Vcs-Git URL.
  * Declare compliance with Debian Policy 3.9.8.
  * Drop CDBS get-orig-source target: Use "gbp import-orig --uscan" instead.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend copyright of packaging to cover current year.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 07 May 2016 11:37:06 +0200

leaflet-markercluster (0.4.0~dfsg-1) unstable; urgency=medium

  * Use recent Uglifyjs:
    + Drop patch 2002.
    + Tighten to build-depend versioned on node-uglify.
  * Update Vcs-Browser URL to use cgit web frontend.
  * Declare compliance with Debian Policy 3.9.6.
  * Add alternate git source URL.
  * Update upstream version numbering scheme in watch file and
    upstream-tarball hints in rules file.
  * Stop tracking md5sum of upstream tarball.
  * Update copyright info:
    + Extend coverage for myself to include current year.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 16 Oct 2014 17:54:43 +0200

leaflet-markercluster (0.4~dfsg-2) unstable; urgency=medium

  * Fix use common install path for example files.
    Also fixes applying path adjustments to those example files.
  * Install together with Leaflet (not in separate dir).
  * Fix cleanup uglified code in clean target.
  * Fix invoke jake only once.
  * Drop examples needing DFSG-nonfree or raw source files.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 01 Feb 2014 15:47:43 +0100

leaflet-markercluster (0.4~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release.
    + Works with leaflet 0.7.1+.
      Closes: bug#733116. Thanks to Martín Ferrari.

  [ Jonas Smedegaard ]
  * Move packaging to pkg-javascript team.
  * Depend on (not recommend) leaflet, and tighten to recent versions.
  * Improve example files path fixing (i.e. Debian relative paths
    instead of CDN) to be version-agnostic.
  * Update copyright file: Cover newly included convenience code copies.
  * Unfuzz and update patch 2001.
  * Add patch 2002 to adapt build script to support Uglifyjs 1.x.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 28 Jan 2014 18:41:34 +0100

leaflet-markercluster (0.2~dfsg-1) unstable; urgency=low

  * Initial release.
  * Closes: bug#694119.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 04 Nov 2013 18:47:05 +0100
